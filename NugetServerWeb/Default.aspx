﻿<%@ Page Language="C#" %>

<%@ Import Namespace="NuGet.Server" %>
<%@ Import Namespace="NuGet.Server.App_Start" %>
<%@ Import Namespace="NuGet.Server.Infrastructure" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>NuGet 私有仓库</title>
    <style type="text/css">
        body {
            font-size: 14pt;
            font-family: 'Microsoft YaHei'
        }

        .box {
            width: 720px;
            height: 480px;
            margin: 8% auto;
            border: 1px solid #0094ff;
            padding: 20px;
        }

            .box > .title {
                font-size: 16pt;
                height: 32pt;
                line-height: 32pt;
                vertical-align: middle;
                color: #0094ff;
                margin-bottom: 10px;
                text-align: center;
            }

        .line {
            height: 32pt;
            line-height: 32pt;
            vertical-align: middle;
        }

            .line > label {
                color: #0094ff;
            }
    </style>
</head>
<body>
    <div class="box">
        <div class="title">
            NuGet 私有仓库
        </div>
        <div class="title">
           点击<a href="<%= VirtualPathUtility.ToAbsolute("~/nuget/Packages") %>">这里</a>查看您的包。
        </div>
        <div class="line">
            <label>仓库版本：</label>
            <span><%= typeof(NuGetODataConfig).Assembly.GetName().Version %></span>
        </div>
        <div class="line">
            <label>仓库名称：</label>
            <span>nuget.grd.cn</span>
        </div>
        <div class="line">
            <label>仓库地址：</label>
            <span>https://47.112.110.46:81/nuget</span>
        </div>
        <div class="line">
            <label>在线上传：</label>
            <span><a href="/upload.aspx" target="_blank">上传页面</a></span>
        </div>
        <div class="line">
            <label>命令上传：</label>
            <span><strong style="font-size:12px;">nuget.exe push {package file} {apikey} -Source https://47.112.110.46:81/nuget</strong></span>
            <span style="font-size:12px;"><a href="https://www.nuget.org/downloads" target="_blank">下载nuget.exe</a></span>
        </div>

        <div class="line">
            <label>仓库使用：</label>
            <span style="font-size:12px;">Visual Studio 菜单：工具(T)- 选项(O)-NuGet程序包管理(左侧导航)-程序包源</span>
            <div>
                <div style="display:inline-block;">
                   <a href="Explain/替换文本.txt">操作说明</a>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
