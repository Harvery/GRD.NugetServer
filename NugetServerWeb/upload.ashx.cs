﻿using Microsoft.Azure.Management.DataFactories.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace NugetServerWeb
{
    /// <summary>
    /// upload 的摘要说明
    /// </summary>
    public class upload : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            //客户端上传的文件
            HttpFileCollection _file = HttpContext.Current.Request.Files;

            string msg = string.Empty;
            string error = string.Empty;
            string result = string.Empty;

            if (_file.Count > 0&& _file.Count <= 31457280)
            {
                //文件大小   
                long size = _file[0].ContentLength;
                //文件类型   
                string type = _file[0].ContentType;
                //文件名   
                string name = _file[0].FileName;
                //文件格式   
                string _tp = System.IO.Path.GetExtension(name);
                //
                if (_tp.ToLower() == ".nupkg")
                {
                    //保存文件   
                    string fileName =  Path.GetFileName(_file[0].FileName);
                    //保存文件
                    _file[0].SaveAs(context.Server.MapPath("~/Packages/" + fileName));

                    msg = "文件上传成功！";
                    result = "{msg:'" + msg + "',fileName:'" + fileName + "'}";
                }
                else
                {
                    error = "文件上传失败！";
                    result = "{ error:'" + error + "'}";
                }
            }

            context.Response.Write(result);
            context.Response.End();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}