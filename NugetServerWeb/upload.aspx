﻿<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>NuGet包上传</title>
    <style>
        body {
            font-size: 14pt;
            font-family: 'Microsoft YaHei'
        }

        .box {
            width: 620px;
            height: 480px;
            margin: 8% auto;
            border: 1px solid #0094ff;
            padding: 20px;
        }

            .box > .title {
                font-size: 16pt;
                height: 32pt;
                line-height: 32pt;
                vertical-align: middle;
                color: #0094ff;
                margin-bottom: 10px;
                text-align: center;
            }

        .line {
            height: 32pt;
            line-height: 32pt;
            vertical-align: middle;
        }

            .line > label {
                color: #0094ff;
            }

        .button, input[type=button], input[type=submit] {
            border: 1px solid #0094ff;
            color: white;
            font-size: 14pt;
            height: 24pt;
            vertical-align: middle;
            background-color: #0094ff
        }

        .log {
            width: 97%;
            height: 300px;
            overflow: auto;
            border: 1px solid #0094ff;
            font-size: 11pt;
            line-height: 11pt;
            padding: 10px;
        }
    </style>
</head>
<body>
    <div class="box">
        <div class="title">
            NuGet 包上传
        </div>

        <form action="upload.ashx" method="post" enctype="multipart/form-data">
            <div class="line">
                <label>文件：</label>
                <input type="file" accept="*.nupkg" name="file" id="file">
            </div>
            <div class="line">
                <input type="submit" value="上 传">
            </div>
        </form>

        <div class="log" style="color: green;">
            <p>
            </p>
        </div>
    </div>
</body>
</html>
